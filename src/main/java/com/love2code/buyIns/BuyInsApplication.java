package com.love2code.buyIns;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BuyInsApplication {

	public static void main(String[] args) {
		SpringApplication.run(BuyInsApplication.class, args);
	}

}
