package com.love2code.buyIns;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class FailedController {

	@Autowired
	FailedRepository failRepository;
	
	@Autowired
	NotificationRepository notificationRepository;
	
	@GetMapping("/report")
	public List<Failed> getfailed() {
		return failRepository.findAll();
	}
	
	@GetMapping("/add")
	public String saveReport() {
		
		Failed fail = new Failed("123","456","suraj");
		Notification notification = new Notification(11,"Notification1");
		
		failRepository.save(fail);
		notificationRepository.save(notification);
		
		return "success";
	}
}
