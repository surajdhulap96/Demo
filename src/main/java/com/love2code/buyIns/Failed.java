package com.love2code.buyIns;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.OneToMany;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
@IdClass(TradeId.class)
public class Failed {

	@Id
	private String tradeId;
	@Id
	private String externalTradeId;
	private String name;
	
	@OneToMany
	private List<Notification> notifications;

	public Failed(String tradeId, String externalTradeId, String name) {
		this.tradeId = tradeId;
		this.externalTradeId = externalTradeId;
		this.name = name;
	}
	
	
}
