package com.love2code.buyIns;

import java.io.Serializable;

import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class TradeId implements Serializable{

	private String tradeId;
	@Id
	private String externalTradeId;
}
