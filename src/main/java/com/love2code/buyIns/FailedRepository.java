package com.love2code.buyIns;

import org.springframework.data.jpa.repository.JpaRepository;

public interface FailedRepository extends JpaRepository<Failed, Long> {

}
